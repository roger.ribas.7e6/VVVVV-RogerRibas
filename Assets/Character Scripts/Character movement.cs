using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Charactermovement : MonoBehaviour
{
    //Variables
    private float horizontal;
    private float speed = 8f;
    private bool isFacingRight = true;
    private bool isAlive = true;
    public static Charactermovement p;

    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private Animator animator;
    [SerializeField] private BoxCollider2D boxCollider;
    [SerializeField] private SpriteRenderer spriteRenderer;


    //Awake
    private void Awake()
    {
        if (p == null)
        {
            p = this;
        }
        else Destroy(this.gameObject);
        DontDestroyOnLoad(p);
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        boxCollider = GetComponent<BoxCollider2D>();
    }


    // Update is called once per frame
    void Update()
    {
        

        horizontal = Input.GetAxisRaw("Horizontal");

        if (Input.GetButtonDown("Jump"))
        {
            Gravity();
        }

        //if (Input.GetButton("Jump") && IsGrounded())
        //{
        //    rb.velocity = new Vector2(rb.velocity.x, jumpingpower);
        //}

        //if (Input.GetButton("Jump") && rb.velocity.y > 0f)
        //{
        //    rb.velocity = new Vector2 (rb.velocity.x, rb.velocity.y * 0.5f );
        //}
        IsOnAir();
        Moving();
        FlipX();

        if (Input.GetButtonDown("Cancel"))
        {
            SceneManager.LoadScene("Pause", LoadSceneMode.Single);
        }

    }



    private void FixedUpdate()
    {
        rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);
    }

    private void Moving()
    {
        if (horizontal == 0f)
        {
            animator.SetBool("Moving", false);
        }
        else
        {
            animator.SetBool("Moving", true);
        }
    }

    private void OnCollisionEnter2D(Collision2D collider)
    {
        Debug.Log(collider.gameObject.name);
        if (collider.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("hola");
            SceneManager.LoadScene("DIE");

        }
    }
    private void ToMenu()
    {
        Debug.Log("Cagaste rey");
    }

    private void Gravity()
    {

        if (IsGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.1f);
            rb.gravityScale = rb.gravityScale * -1;
            rb.rotation += 180;
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }
    }

    private bool IsGrounded()
    {
        return Physics2D.OverlapCircle(groundCheck.position, 0.2f, groundLayer);
    }

    private void FlipX()
    {
        if (isFacingRight && horizontal < 0f || !isFacingRight && horizontal > 0f)
        {
            isFacingRight = !isFacingRight;
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }
    }

    private void IsOnAir()
    {
        if (!IsGrounded())
        {
            animator.SetBool("OnAir", true);
        }
        if (IsGrounded())
        {
            animator.SetBool("OnAir", false);
        }
    }

}
