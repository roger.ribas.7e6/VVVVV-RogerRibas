using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResumeScript : MonoBehaviour
{
    // Start is called before the first frame update
    public void Pausa()
    {
        Time.timeScale = 0f;
    }
    public void Reset()
    {
        Time.timeScale = 1f;
    }
}
