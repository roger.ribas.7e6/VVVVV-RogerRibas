using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using static Unity.Burst.Intrinsics.X86;


public class Scene_Manager : MonoBehaviour
{
    public int scID;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        scID = SceneManager.GetActiveScene().buildIndex;
        if (collision.name == "Player")
        {
            print("Trigger "+scID);
             if (this.tag == "NextScene")
            {
                SceneManager.LoadScene(scID + 1, LoadSceneMode.Single);
            }
            else if (this.tag == "ReturnScene")
            {
                SceneManager.LoadScene(scID - 1, LoadSceneMode.Single);
            }
        }
        
    }
    
    
}
